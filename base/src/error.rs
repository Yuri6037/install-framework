// Copyright 2021 Yuri6037

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

use std::io;
use std::num::ParseIntError;
use zip::result::ZipError;
use reqwest::header::InvalidHeaderName;
use reqwest::header::InvalidHeaderValue;

pub trait Error
{
    type ErrorType;

    fn generic(e: String) -> Self::ErrorType;
    fn io(e: io::Error) -> Self::ErrorType;
    fn zip(e: ZipError) -> Self::ErrorType;
    fn network(e: reqwest::Error) -> Self::ErrorType;
    fn network_invalid_header_name(e: InvalidHeaderName) -> Self::ErrorType;
    fn network_invalid_header_value(e: InvalidHeaderValue) -> Self::ErrorType;
    fn parse_int(e: ParseIntError) -> Self::ErrorType;
    fn unknown_resource(resid: usize) -> Self::ErrorType;
    fn unknown_resource_path(res: &str) -> Self::ErrorType;
}
