// Copyright 2021 Yuri6037

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

use std::collections::HashMap;
use std::string::String;
use reqwest::blocking::Client;
use install_framework_core::command::Interpreter;
use install_framework_core::command::InitCommand;

use super::base;
use crate::interface::SimpleInterpreter;
use crate::cache::Cache;
use crate::error::Error;

pub struct InitInterpreter<'a, TError: Error>
{
    interpreter: &'a mut dyn SimpleInterpreter<TError::ErrorType>,
    cache: &'a mut Cache<TError>,
    props: &'a HashMap<String, String>,
    client: Client,
    resources: &'a HashMap<&'static str, &'static [u8]>
}

impl <'a, TError: Error> InitInterpreter<'a, TError>
{
    pub fn new(interpreter: &'a mut dyn SimpleInterpreter<TError::ErrorType>, cache: &'a mut Cache<TError>, props: &'a HashMap<String, String>, resources: &'a HashMap<&'static str, &'static [u8]>) -> InitInterpreter<'a, TError>
    {
        return InitInterpreter
        {
            interpreter: interpreter,
            cache: cache,
            props: props,
            client: Client::new(),
            resources: resources
        }
    }

    fn download_file(&mut self, resid: usize, filename: String, url: String, headers: HashMap<String, String>) -> Result<(), TError::ErrorType>
    {
        return base::download_file(self.interpreter, &mut self.client, &mut self.cache, resid, filename, url, headers);
    }

    fn unpack_cached(&mut self, resid: usize, path: String) -> Result<(), TError::ErrorType>
    {
        return base::unpack_cached(self.interpreter, &mut self.cache, resid, path);
    }

    fn extract_resource(&mut self, resid: usize, path: &'static str) -> Result<(), TError::ErrorType>
    {
        return base::extract_resource(&mut self.cache, &self.resources, resid, path);
    }

    fn user_input(&mut self, resid: usize, prop: String, message: String) -> Result<(), TError::ErrorType>
    {
        return base::user_input(self.interpreter, &mut self.cache, &self.props, resid, prop, message);
    }
}

impl <TError: Error> Interpreter<InitCommand> for InitInterpreter<'_, TError>
{
    type ErrorType = TError::ErrorType;

    fn execute(&mut self, resid: usize, cmd: InitCommand) -> Result<(), Self::ErrorType>
    {
        match cmd
        {
            InitCommand::DownloadFile(f, u, h) => self.download_file(resid, f, u, h)?,
            InitCommand::UnpackCached(p) => self.unpack_cached(resid, p)?,
            InitCommand::ExtractResource(p) => self.extract_resource(resid, p)?,
            InitCommand::UserInput(p, m) => self.user_input(resid, p, m)?
        }
        return Ok(());
    }

    fn progress(&mut self, cur: usize, max: usize) -> Result<(), Self::ErrorType>
    {
        self.interpreter.update_step((cur as f64 / max as f64) as f32)?;
        return Ok(());
    }
}
