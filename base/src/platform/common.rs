// Copyright 2021 Yuri6037

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

#[cfg(target_os="linux")]
use std::path::Path;
use std::path::PathBuf;
use install_framework_core::interface::InstallMethod;

#[cfg(target_os="linux")]
pub fn get_desktop_folder(install_dir: &Path, method: InstallMethod) -> PathBuf
{
    match method
    {
        InstallMethod::SystemInstall => return PathBuf::from("/usr/share/applications"),
        InstallMethod::UserInstall => return install_dir.join("share").join("applications")
    }
}

#[cfg(windows)]
pub fn get_desktop_folder(method: InstallMethod) -> PathBuf
{
    use winapi::shared::windef::HWND;
    use winapi::shared::ntdef::NULL;
    use winapi::shared::minwindef::MAX_PATH;
    use winapi::um::shlobj::SHGetSpecialFolderPathW;
    use winapi::um::shlobj::CSIDL_COMMON_PROGRAMS;
    use winapi::um::shlobj::CSIDL_PROGRAMS;
    use winapi::um::winnt::WCHAR;
    use winapi::shared::minwindef::FALSE;
    use std::ffi::OsString;
    use std::os::windows::ffi::OsStringExt;

    let mut buf: [WCHAR; MAX_PATH] = [0; MAX_PATH];
    match method
    {
        InstallMethod::SystemInstall =>
        {
            unsafe
            {
                if SHGetSpecialFolderPathW(NULL as HWND, buf.as_mut_ptr(), CSIDL_COMMON_PROGRAMS, FALSE) == FALSE
                {
                    panic!("Your system is insane: you do not have any system start menu folder! You cannot use Install-Framework!");
                }    
            }
        },
        InstallMethod::UserInstall =>
        {
            unsafe
            {
                if SHGetSpecialFolderPathW(NULL as HWND, buf.as_mut_ptr(), CSIDL_PROGRAMS, FALSE) == FALSE
                {
                    panic!("Your system is insane: you do not have any start menu folder! You cannot use Install-Framework!");
                }
            }
        }
    };
    let len = buf.iter().take_while(|&&c| c != 0).count();
    return PathBuf::from(OsString::from_wide(&buf[..len]));
}

pub fn get_target_desktop_name(path: &str) -> String
{
    let name_with_ext =
    {
        if let Some(id) = path.rfind('/')
        {
            &path[id + 1..]
        }
        else
        {
            path
        }
    };
    let name =
    {
        if let Some(id) = name_with_ext.rfind('.')
        {
            &name_with_ext[0..id]
        }
        else
        {
            name_with_ext
        }
    };
    return String::from(name);
}
