// Copyright 2021 Yuri6037

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

use std::path::Path;
use install_framework_core::interface::InstallMethod;

use super::registry;
use crate::error::Error;

#[cfg(windows)]
pub fn remove_path<TError: Error>(bin_dir: &Path, method: InstallMethod) -> Result<(), TError::ErrorType>
{
    let value = match method
    {
        InstallMethod::UserInstall => registry::get_path(true),
        InstallMethod::SystemInstall => registry::get_path(false)
    };
    match value
    {
        Err(e) => return Err(TError::generic(format!("Error accessing registry: {:?}", e))),
        Ok(mut v) =>
        {
            let old = v.len();
            v.retain(|d|
            {
                return d != bin_dir.as_os_str();
            });
            if v.len() == old
            {
                return Ok(());
            }
            let res = match method
            {
                InstallMethod::UserInstall => registry::set_path(true, v),
                InstallMethod::SystemInstall => registry::set_path(false, v)
            };
            if let Err(e) = res
            {
                return Err(TError::generic(format!("Error accessing registry: {:?}", e)));
            }
        }
    };
    return Ok(());
}

#[cfg(windows)]
pub fn append_path<TError: Error>(bin_dir: &Path, method: InstallMethod) -> Result<(), TError::ErrorType>
{
    use std::ffi::OsString;

    let value = match method
    {
        InstallMethod::UserInstall => registry::get_path(true),
        InstallMethod::SystemInstall => registry::get_path(false)
    };
    match value
    {
        Err(e) => return Err(TError::generic(format!("Error accessing registry: {:?}", e))),
        Ok(mut v) =>
        {
            for p in &v
            {
                if p == bin_dir.as_os_str()
                {
                    return Ok(());
                }
            }
            v.push(OsString::from(bin_dir.as_os_str()));
            let res = match method
            {
                InstallMethod::UserInstall => registry::set_path(true, v),
                InstallMethod::SystemInstall => registry::set_path(false, v)
            };
            if let Err(e) = res
            {
                return Err(TError::generic(format!("Error accessing registry: {:?}", e)));
            }
        }
    };
    return Ok(());
}
