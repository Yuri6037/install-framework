// Copyright 2021 Yuri6037

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

use std::io;
use zip::result::ZipError;
use install_framework_base::error::Error;
use reqwest::header::InvalidHeaderName;
use reqwest::header::InvalidHeaderValue;
use std::num::ParseIntError;

pub enum CliError
{
    Io(io::Error),
    Generic(String),
    Skip,
    Network(reqwest::Error),
    Zip(ZipError)
}

impl Error for CliError
{
    type ErrorType = CliError;

    fn generic(e: std::string::String) -> CliError
    {
        return CliError::Generic(e);
    }

    fn io(e: std::io::Error) -> CliError
    {
        return CliError::Io(e);
    }

    fn zip(e: ZipError) -> CliError
    {
        return CliError::Zip(e);
    }

    fn network(e: reqwest::Error) -> CliError
    {
        return CliError::Network(e);
    }

    fn network_invalid_header_name(e: InvalidHeaderName) -> CliError
    {
        return CliError::Generic(format!("Invalid header name: {}", e));
    }

    fn network_invalid_header_value(e: InvalidHeaderValue) -> CliError
    {
        return CliError::Generic(format!("Invalid header value: {}", e));
    }

    fn parse_int(e: ParseIntError) -> CliError
    {
        return CliError::Generic(format!("Invalid integer string: {}", e));
    }

    fn unknown_resource(resid: usize) -> CliError
    {
        return CliError::Generic(format!("Unknown resource id: {}", resid));
    }

    fn unknown_resource_path(res: &str) -> CliError
    {
        return CliError::Generic(format!("Unknown resource path: {}", res));
    }
}
