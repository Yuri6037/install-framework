// Copyright 2021 Yuri6037

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

use std::io;
use std::io::BufRead;
use std::string::String;
use install_framework_base::interface::SimpleInterpreter;

use crate::error::CliError;

pub struct CliInterpreter
{
    step: String,
    substep: String
}

impl CliInterpreter
{
    pub fn new() -> CliInterpreter
    {
        return CliInterpreter
        {
            step: String::new(),
            substep: String::new()
        }
    }
}

pub fn read_console() -> Result<String, CliError>
{
    if let Some(res) = io::stdin().lock().lines().next()
    {
        match res
        {
            Ok(data) => return Ok(data),
            Err(e) => return Err(CliError::Io(e))
        };
    }
    return Err(CliError::Generic(String::from("Could not read line from console")));
}

impl SimpleInterpreter<CliError> for CliInterpreter
{
    fn read_user_input_text(&mut self, msg: &str) -> Result<String, CliError>
    {
        println!("{}:", msg);
        return read_console();
    }

    fn begin_step(&mut self, message: &str) -> Result<(), CliError>
    {
        println!("{}", message);
        self.step = String::from(message);
        return Ok(());
    }

    fn update_step(&mut self, value: f32) -> Result<(), CliError>
    {
        println!("{} ({:.2}%)", self.step, value * 100.0);
        return Ok(());
    }

    fn end_step(&mut self) -> Result<(), CliError>
    {
        println!("{} (OK)", self.step);
        return Ok(());
    }

    fn begin_substep(&mut self, message: &str) -> Result<(), CliError>
    {
        print!("{}\r", message);
        self.substep = String::from(message);
        return Ok(());
    }

    fn update_substep(&mut self, value: f32) -> Result<(), CliError>
    {
        print!("{} ({:.2}%)\r", self.substep, value * 100.0);
        return Ok(());
    }

    fn end_substep(&mut self) -> Result<(), CliError>
    {
        //Spaces are here to clear the terminal line
        println!("{} (OK)                   ", self.substep);
        return Ok(());
    }
}
