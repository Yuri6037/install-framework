// Copyright 2021 Yuri6037

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

use std::path::PathBuf;
use dirs::data_local_dir;

use crate::interface::InstallMethod;

#[cfg(unix)]
pub fn get_install_dir(method: &InstallMethod) -> PathBuf
{
    match method
    {
        InstallMethod::SystemInstall => return PathBuf::from("/usr/local"),
        InstallMethod::UserInstall =>
        {
            match data_local_dir()
            {
                Some(o) => return o.join(".."), //Come back one directory as the root install should be ~/.local as there might be stuff to place in bin
                None => panic!("Your system is insane: you do not have any local user data directory, as such you cannot install apps as your local user!\nYou may try a system-wide install if this installer permits it.")
            }
        }
    }
}

#[cfg(windows)]
pub fn get_install_dir(method: &InstallMethod) -> PathBuf
{
    use std::ffi::OsString;
    use std::os::windows::ffi::OsStringExt;
    use winapi::um::winnt::WCHAR;
    use winapi::shared::minwindef::MAX_PATH;
    use winapi::um::shlobj::SHGetSpecialFolderPathW;
    use winapi::um::shlobj::CSIDL_PROGRAM_FILES;
    use winapi::shared::minwindef::FALSE;
    use winapi::shared::ntdef::NULL;
    use winapi::shared::windef::HWND;

    match method
    {
        InstallMethod::SystemInstall =>
        {
            let mut buf: [WCHAR; MAX_PATH] = [0; MAX_PATH];
            unsafe
            {
                if SHGetSpecialFolderPathW(NULL as HWND, buf.as_mut_ptr(), CSIDL_PROGRAM_FILES, FALSE) == FALSE
                {
                    panic!("Your system is insane: you do not have any system program folder, as such you cannot install apps system wide!\nYou may try a user install if this installer permits it.");
                }
                let len = buf.iter().take_while(|&&c| c != 0).count();
                return PathBuf::from(OsString::from_wide(&buf[..len]));
            }
        }
        InstallMethod::UserInstall =>
        {
            match data_local_dir()
            {
                Some(o) => return o.join(".."), //Come back one directory as the root install should be ~/.local as there might be stuff to place in bin
                None => panic!("Your system is insane: you do not have any local user data directory, as such you cannot install apps as your local user!\nYou may try a system-wide install if this installer permits it.")
            }
        }
    }
}
