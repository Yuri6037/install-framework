// Copyright 2021 Yuri6037

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

use std::io;
use zip::result::ZipError;
use install_framework_base::error::Error;
use reqwest::header::InvalidHeaderName;
use reqwest::header::InvalidHeaderValue;
use std::num::ParseIntError;
use async_channel::SendError;
use async_channel::RecvError;

pub enum GuiError
{
    Io(io::Error),
    Generic(String),
    Network(reqwest::Error),
    Zip(ZipError),
    Channel(String),
    Interupted,
    IllegalMessage
}

impl GuiError
{
    pub fn translate(&self) -> (String, i32)
    {
        match self
        {
            GuiError::Generic(s) => return (format!("A generic error has occured: {}", s), 1),
            GuiError::Io(e) => return (format!("An IO error has occured: {}", e), 2),
            GuiError::Interupted => return (String::from("Interrupted!"), 0),
            GuiError::IllegalMessage => return (String::from("Internal application error: illegal message received"), 7),
            GuiError::Network(e) => return (format!("A network error has occured: {}", e), 3),
            GuiError::Zip(e) => return (format!("A zip error has occured: {}", e), 4),
            GuiError::Channel(e) => return (format!("Internal application error: channel i/o failure: {}", e), 7)
        }    
    }

    pub fn channel_send<T>(e: SendError<T>) -> GuiError
    {
        return GuiError::Channel(format!("failed to send message: {}", e));
    }

    pub fn channel_recv(e: RecvError) -> GuiError
    {
        return GuiError::Channel(format!("failed to receive message: {}", e));
    }
}

impl Error for GuiError
{
    type ErrorType = GuiError;

    fn generic(e: std::string::String) -> GuiError
    {
        return GuiError::Generic(e);
    }

    fn io(e: std::io::Error) -> GuiError
    {
        return GuiError::Io(e);
    }

    fn zip(e: ZipError) -> GuiError
    {
        return GuiError::Zip(e);
    }

    fn network(e: reqwest::Error) -> GuiError
    {
        return GuiError::Network(e);
    }

    fn network_invalid_header_name(e: InvalidHeaderName) -> GuiError
    {
        return GuiError::Generic(format!("Invalid header name: {}", e));
    }

    fn network_invalid_header_value(e: InvalidHeaderValue) -> GuiError
    {
        return GuiError::Generic(format!("Invalid header value: {}", e));
    }

    fn parse_int(e: ParseIntError) -> GuiError
    {
        return GuiError::Generic(format!("Invalid integer string: {}", e));
    }

    fn unknown_resource(resid: usize) -> GuiError
    {
        return GuiError::Generic(format!("Unknown resource id: {}", resid));
    }

    fn unknown_resource_path(res: &str) -> GuiError
    {
        return GuiError::Generic(format!("Unknown resource path: {}", res));
    }
}
