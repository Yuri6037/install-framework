// Copyright 2021 Yuri6037

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

use async_channel::Sender;
use async_channel::SendError;
use async_channel::TrySendError;
use async_channel::RecvError;
use async_channel::Receiver;
use async_channel::TryRecvError;

//Extension for async_channel
pub trait SenderSync<T>
{
    fn send_sync(&self, msg: T) -> Result<(), SendError<T>>;
}

pub trait ReceiverSync<T>
{
    fn recv_sync(&self) -> Result<T, RecvError>;
}

impl <T> SenderSync<T> for Sender<T>
{
    fn send_sync(&self, mut msg: T) -> Result<(), SendError<T>>
    {
        loop
        {
            match self.try_send(msg)
            {
                Ok(()) => return Ok(()),
                Err(e) =>
                {
                    match e
                    {
                        TrySendError::<T>::Closed(v) => return Err(SendError::<T>(v)),
                        TrySendError::<T>::Full(v) => msg = v
                    };
                }
            };
        }
    }
}

impl <T> ReceiverSync<T> for Receiver<T>
{
    fn recv_sync(&self) -> Result<T, RecvError>
    {
        loop
        {
            match self.try_recv()
            {
                Ok(v) => return Ok(v),
                Err(e) =>
                {
                    match e
                    {
                        TryRecvError::Closed => return Err(RecvError {}),
                        _ => ()
                    };
                }
            };
        }
    }
}
