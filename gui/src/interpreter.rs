// Copyright 2021 Yuri6037

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

use std::string::String;
use async_channel::Sender;
use async_channel::Receiver;
use install_framework_base::interface::SimpleInterpreter;

use crate::messages::ThreadMessage;
use crate::messages::RenderMessage;
use crate::error::GuiError;
use crate::ext::ReceiverSync;
use crate::ext::SenderSync;

pub struct ThreadedInterpreter
{
    sender: Sender<RenderMessage>,
    receiver: Receiver<ThreadMessage>
}

impl ThreadedInterpreter
{
    pub fn new(sender: Sender<RenderMessage>, receiver: Receiver<ThreadMessage>) -> ThreadedInterpreter
    {
        return ThreadedInterpreter
        {
            sender: sender,
            receiver: receiver
        };
    }
}

impl SimpleInterpreter<GuiError> for ThreadedInterpreter
{
    fn read_user_input_text(&mut self, msg: &str) -> Result<String, GuiError>
    {
        if let Err(e) = self.sender.send_sync(RenderMessage::UserInput(String::from(msg)))
        {
            return Err(GuiError::channel_send(e));
        }
        match self.receiver.recv_sync()
        {
            Ok(v) =>
            {
                match v
                {
                    ThreadMessage::UserInput(v) => return Ok(v),
                    ThreadMessage::Terminate => return Err(GuiError::Interupted),
                    _ => return Err(GuiError::IllegalMessage)
                }
                
            },
            Err(e) => return Err(GuiError::channel_recv(e))
        }
    }

    fn begin_step(&mut self, msg: &str) -> Result<(), GuiError>
    {
        if let Err(e) = self.sender.send_sync(RenderMessage::BeginStep(String::from(msg)))
        {
            return Err(GuiError::channel_send(e));
        }
        return Ok(());
    }
    fn update_step(&mut self, v: f32) -> Result<(), GuiError>
    {
        if let Err(e) = self.sender.send_sync(RenderMessage::UpdateStep(v))
        {
            return Err(GuiError::channel_send(e));
        }
        return Ok(());
    }

    fn end_step(&mut self) -> Result<(), GuiError>
    {
        return Ok(());
    }

    fn begin_substep(&mut self, msg: &str) -> Result<(), GuiError>
    {
        if let Err(e) = self.sender.send_sync(RenderMessage::BeginSubstep(String::from(msg)))
        {
            return Err(GuiError::channel_send(e));
        }
        return Ok(());
    }

    fn update_substep(&mut self, v: f32) -> Result<(), GuiError>
    {
        if let Err(e) = self.sender.send_sync(RenderMessage::UpdateSubstep(v))
        {
            return Err(GuiError::channel_send(e));
        }
        return Ok(());
    }

    fn end_substep(&mut self) -> Result<(), GuiError>
    {
        if let Err(e) = self.sender.send_sync(RenderMessage::EndSubstep)
        {
            return Err(GuiError::channel_send(e));
        }
        return Ok(());
    }
}
