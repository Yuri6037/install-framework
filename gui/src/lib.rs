// Copyright 2021 Yuri6037

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

use install_framework_core::interface::ConstructibleInterface;
use install_framework_core::builder::InstallerBuilder;
use async_channel::unbounded;

mod messages;
mod error;
mod interpreter;
mod renderer;
mod interface;
mod ext;

pub use interface::GuiInterface;
use messages::RenderMessage;
use messages::ThreadMessage;

impl ConstructibleInterface<'_> for GuiInterface
{
    fn run(builder: InstallerBuilder) -> i32
    {
        let code = crossbeam::scope(move |s|
        {
            let (sender, receiver) = unbounded::<RenderMessage>();
            let (sender1, receiver1) = unbounded::<ThreadMessage>();
            let handle = s.spawn(|_|
            {
                let link = interface::ThreadLinker::new(sender, receiver1);
                let mut interface = interface::GuiInterface::new(link);
                let mut b = builder;
                return b.run(&mut interface);
            });
            if let Err(e) = renderer::rendering_loop(sender1, receiver)
            {
                eprintln!("GUI engine failure {}", e);
                return 5;
            }
            match handle.join()
            {
                Ok(v) => return v,
                Err(e) =>
                {
                    eprintln!("Internal application error: thread panic: {:?}", e);
                    return 7;
                }
            };
        }).unwrap();
        return code;
    }
}
