// Copyright 2021 Yuri6037

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

use async_channel::Sender;
use async_channel::SendError;
use std::string::String;
use iced::Element;
use iced::Column;
use iced::Text;
use iced::Row;
use iced::button;
use iced::Length;
use iced::Align;

use super::window::Page;
use super::window::ConstructiblePage;
use super::window::PageMessage;
use crate::messages::RenderMessage;
use crate::messages::ThreadMessage;
use crate::messages;

pub struct ErrorPage
{
    code: i32,
    msg: String,
    useless: button::State
}

impl ConstructiblePage for ErrorPage
{
    type PageType = ErrorPage;

    fn new(msg: &messages::Page) -> ErrorPage
    {
        if let messages::Page::Error(e) = msg
        {
            let (err, code) = e.translate();
            return ErrorPage
            {
                code: code,
                msg: err,
                useless: button::State::new()
            };
        }
        panic!("The impossible happened: incorrect message received");
    }
}

impl Page for ErrorPage
{
    fn handle_message(&mut self, _: &RenderMessage, _: &mut Sender<ThreadMessage>) -> Result<(), SendError<ThreadMessage>>
    {
        return Ok(());
    }

    fn update(&mut self, msg: &PageMessage, _: &mut Sender<ThreadMessage>) -> Result<bool, SendError<ThreadMessage>>
    {
        match msg
        {
            PageMessage::Close => return Ok(true),
            _ => ()
        };
        return Ok(false);
    }

    fn view(&mut self) -> Element<PageMessage>
    {
        return Column::new()
            .push(Text::new("An error has occured"))
            .push(Row::new().push(Text::new("Error code: ")).push(Text::new(self.code.to_string())))
            .push(Text::new("Error message:"))
            .push(Text::new(&self.msg))
            .push(
                Column::new()
                    .width(Length::Fill)
                    .align_items(Align::End)
                    .push(button::Button::new(&mut self.useless, Text::new("Close")).on_press(PageMessage::Close))
            )
            .into();
    }
}
