// Copyright 2021 Yuri6037

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

use std::string::String;
use async_channel::Sender;
use async_channel::SendError;
use iced::Element;
use iced::Column;
use iced::Text;
use iced::button;
use iced::Length;
use iced::Align;

use super::window::Page;
use super::window::ConstructiblePage;
use super::window::PageMessage;
use crate::messages::RenderMessage;
use crate::messages::ThreadMessage;
use crate::messages;

pub struct FinishPage
{
    useless: button::State,
    message: String
}

impl ConstructiblePage for FinishPage
{
    type PageType = FinishPage;

    fn new(page: &messages::Page) -> FinishPage
    {
        if let messages::Page::Finish(msg) = page
        {
            return FinishPage
            {
                useless: button::State::new(),
                message: msg.clone()
            };
        }
        panic!("The impossible happened: incorrect message received");
    }
}

impl Page for FinishPage
{
    fn handle_message(&mut self, _: &RenderMessage, _: &mut Sender<ThreadMessage>) -> Result<(), SendError<ThreadMessage>>
    {
        return Ok(());
    }

    fn update(&mut self, msg: &PageMessage, _: &mut Sender<ThreadMessage>) -> Result<bool, SendError<ThreadMessage>>
    {
        match msg
        {
            PageMessage::Close => return Ok(true),
            _ => ()
        };
        return Ok(false);
    }

    fn view(&mut self) -> Element<PageMessage>
    {
        return Column::new()
            .spacing(5)
            .push(Text::new(&self.message))
            .push(Text::new(" "))
            .push(Text::new(" "))
            .push(
                Column::new()
                    .width(Length::Fill)
                    .align_items(Align::Center)
                    .push(button::Button::new(&mut self.useless, Text::new("Close")).on_press(PageMessage::Close))
            )
            .into();
    }
}
