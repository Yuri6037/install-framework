// Copyright 2021 Yuri6037

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

use async_channel::Sender;
use async_channel::Receiver;
use iced::Application;
use iced::Settings;

use crate::messages::RenderMessage;
use crate::messages::ThreadMessage;

mod queue;
mod window;
mod welcome;
mod error;
mod processing;
mod home;
mod finish;

pub fn rendering_loop(sender: Sender<ThreadMessage>, receiver: Receiver<RenderMessage>) -> iced::Result
{
    let mut settings = Settings::with_flags((sender, receiver));
    settings.window.size = (360, 550);
    settings.exit_on_close_request = false;
    return window::InstallerWindow::run(settings);
}
