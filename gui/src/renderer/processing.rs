// Copyright 2021 Yuri6037

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

use std::string::String;
use async_channel::Sender;
use async_channel::SendError;
use iced::Element;
use iced::Row;
use iced::Column;
use iced::Text;
use iced::Align;
use iced::button;
use iced::ProgressBar;
use iced::text_input;

use super::window::Page;
use super::window::ConstructiblePage;
use super::window::PageMessage;
use crate::messages::RenderMessage;
use crate::messages::ThreadMessage;
use crate::messages;
use crate::ext::SenderSync;

pub struct Progress
{
    message: String,
    value: f32
}

pub struct ProcessingPage
{
    step: Option<Progress>,
    substep: Option<Progress>,
    user_input_msg: Option<String>,
    user_input_text: String,
    useless: button::State,
    useless1: text_input::State
}

impl ConstructiblePage for ProcessingPage
{
    type PageType = ProcessingPage;

    fn new(msg: &messages::Page) -> ProcessingPage
    {
        if let messages::Page::Processing = msg
        {
            return ProcessingPage
            {
                step: None,
                substep: None,
                user_input_msg: None,
                user_input_text: String::new(),
                useless: button::State::new(),
                useless1: text_input::State::new()
            };
        }
        panic!("The impossible happened: incorrect message received");
    }
}

impl Page for ProcessingPage
{
    fn handle_message(&mut self, msg: &RenderMessage, _: &mut Sender<ThreadMessage>) -> Result<(), SendError<ThreadMessage>>
    {
        match msg
        {
            RenderMessage::UserInput(s) => self.user_input_msg = Some(s.clone()),
            RenderMessage::BeginStep(s) =>
            {
                self.step = Some(Progress
                {
                    message: s.clone(),
                    value: 0.0
                });
            },
            RenderMessage::BeginSubstep(s) =>
            {
                self.substep = Some(Progress
                {
                    message: s.clone(),
                    value: 0.0
                });
            },
            RenderMessage::UpdateSubstep(v) =>
            {
                if let Some(progress) = &mut self.substep
                {
                    progress.value = *v;
                }
            },
            RenderMessage::UpdateStep(v) =>
            {
                if let Some(progress) = &mut self.step
                {
                    progress.value = *v;
                }
            },
            RenderMessage::EndSubstep => self.substep = None,
            _ => ()
        }
        return Ok(());
    }

    fn update(&mut self, msg: &PageMessage, sender: &mut Sender<ThreadMessage>) -> Result<bool, SendError<ThreadMessage>>
    {
        match msg
        {
            PageMessage::Next =>
            {
                self.user_input_msg = None;
                let s = std::mem::replace(&mut self.user_input_text, String::new());
                sender.send_sync(ThreadMessage::UserInput(s))?;
            },
            PageMessage::TextChanged(s) => self.user_input_text = s.clone(),
            _ => ()
        }
        return Ok(false);
    }

    fn view(&mut self) -> Element<PageMessage>
    {
        let mut col = Column::new().spacing(5);
        if let Some(progress) = &self.step
        {
            col = col.push(Text::new(&progress.message));
            col = col.push(
                Row::new()
                    .align_items(Align::Center)
                    .push(ProgressBar::new(0.0..=1.0, progress.value))
                    .push(Text::new(format!("({:.2}%)", progress.value * 100.0)))
            );
            col = col.push(Text::new(" "));
        }
        if let Some(progress) = &self.substep
        {
            col = col.push(Text::new(&progress.message));
            col = col.push(
                Row::new()
                    .align_items(Align::Center)
                    .push(ProgressBar::new(0.0..=1.0, progress.value))
                    .push(Text::new(format!("({:.2}%)", progress.value * 100.0)))
            );
            col = col.push(Text::new(" "));
        }
        if let Some(msg) = &self.user_input_msg
        {
            col = col.push(Text::new(msg));
            col = col.push(
                Row::new()
                    .align_items(Align::Center)
                    .push(text_input::TextInput::new(&mut self.useless1, "", &self.user_input_text, PageMessage::TextChanged))
                    .push(button::Button::new(&mut self.useless, Text::new("OK")).on_press(PageMessage::Next))
            );
        }
        return col.into();
    }
}
