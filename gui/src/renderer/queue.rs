// Copyright 2021 Yuri6037

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

use std::boxed::Box;
use std::hash::Hasher;
use async_channel::Receiver;
use std::hash::Hash;
use std::any::TypeId;
use iced_futures::subscription::Recipe;
use iced_futures::futures::stream::unfold;
use iced_futures::futures::stream::BoxStream;

use crate::error::GuiError;
use crate::messages::RenderMessage;
use crate::messages::Page;

enum State
{
    Running(Receiver<RenderMessage>),
    Terminate
}

pub struct MessageQueue
{
    pub channel: Receiver<RenderMessage>
}

impl <H: Hasher, I> Recipe<H, I> for MessageQueue
{
    type Output = RenderMessage;

    fn hash(&self, state: &mut H)
    {
        TypeId::of::<Self>().hash(state);
    }

    fn stream(self: Box<Self>, _: BoxStream<'static, I>) -> BoxStream<'static, Self::Output>
    {
        return Box::pin(unfold(State::Running(self.channel), |state| async move
        {
            match state
            {
                State::Running(channel) =>
                {
                    match channel.recv().await
                    {
                        Ok(msg) =>
                        {
                            match msg
                            {
                                RenderMessage::SwitchPage(Page::Finish(_)) => return Some((msg, State::Terminate)),
                                RenderMessage::SwitchPage(Page::Error(_)) => return Some((msg, State::Terminate)),
                                _ => return Some((msg, State::Running(channel)))
                            };
                        },
                        Err(e) => return Some((RenderMessage::SwitchPage(Page::Error(GuiError::channel_recv(e))), State::Terminate))
                    };
                },
                State::Terminate =>
                {
                    let _: () = iced::futures::future::pending().await;
                    return None;
                }
            }
        }));
    }
}
