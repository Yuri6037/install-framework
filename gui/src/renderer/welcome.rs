// Copyright 2021 Yuri6037

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

use async_channel::Sender;
use async_channel::SendError;
use iced::Element;
use iced::Row;
use iced::Column;
use iced::Text;
use iced::button;
use iced::Radio;
use iced::Length;
use iced::Align;
use install_framework_core::interface::InstallMethod;

use super::window::Page;
use super::window::ConstructiblePage;
use super::window::PageMessage;
use crate::messages::RenderMessage;
use crate::messages::ThreadMessage;
use crate::messages;
use crate::ext::SenderSync;

pub struct WelcomePage
{
    name: &'static str,
    version: &'static str,
    author: &'static str,
    useless: button::State,
    useless1: button::State,
    method: InstallMethod,
    install_flag: bool
}

impl ConstructiblePage for WelcomePage
{
    type PageType = WelcomePage;

    fn new(page: &messages::Page) -> WelcomePage
    {
        if let messages::Page::Welcome(n, v, a) = page
        {
            return WelcomePage
            {
                name: n,
                version: v,
                author: a,
                useless: button::State::new(),
                useless1: button::State::new(),
                method: InstallMethod::UserInstall,
                install_flag: true
            };
        }
        panic!("The impossible happened: incorrect message received");
    }
}

impl WelcomePage
{
    fn is_user_install(&self) -> bool
    {
        if let InstallMethod::UserInstall = self.method
        {
            return true;
        }
        return false;
    }
}

impl Page for WelcomePage
{
    fn handle_message(&mut self, _: &RenderMessage, _: &mut Sender<ThreadMessage>) -> Result<(), SendError<ThreadMessage>>
    {
        return Ok(());
    }

    fn update(&mut self, msg: &PageMessage, sender: &mut Sender<ThreadMessage>) -> Result<bool, SendError<ThreadMessage>>
    {
        match msg
        {
            PageMessage::UserInstallSelected(b) =>
            {
                if *b
                {
                    self.method = InstallMethod::UserInstall;
                }
                else
                {
                    self.method = InstallMethod::SystemInstall;
                }
            },
            PageMessage::InstallFlagSelected(b) => self.install_flag = *b,
            PageMessage::Next =>
            {
                sender.send_sync(ThreadMessage::Next(Some((self.method, !self.install_flag))))?;
            },
            PageMessage::Close => return Ok(true),
            _ => ()
        };
        return Ok(false);
    }

    fn view(&mut self) -> Element<PageMessage>
    {
        return Column::new()
            .spacing(5)
            .push(Row::new().push(Text::new("Name: ")).push(Text::new(self.name)))
            .push(Row::new().push(Text::new("Version: ")).push(Text::new(self.version)))
            .push(Row::new().push(Text::new("Author: ")).push(Text::new(self.author)))
            .push(Radio::new(true, "Install for current user", Some(self.is_user_install()), PageMessage::UserInstallSelected))
            .push(Radio::new(false, "Install for all users (requires admin)", Some(self.is_user_install()), PageMessage::UserInstallSelected))
            .push(Radio::new(true, "Install some components", Some(self.install_flag), PageMessage::InstallFlagSelected))
            .push(Radio::new(false, "Uninstall some components", Some(self.install_flag), PageMessage::InstallFlagSelected))
            .push(Text::new(" "))
            .push(Text::new(" "))
            .push(
                Row::new()
                    .width(Length::Fill)
                    .push(
                        Column::new()
                            .width(Length::Fill)
                            .align_items(Align::Start)
                            .push(button::Button::new(&mut self.useless, Text::new("Close")).on_press(PageMessage::Close))
                    )
                    .push(
                        Column::new()
                            .width(Length::Fill)
                            .align_items(Align::End)
                            .push(button::Button::new(&mut self.useless1, Text::new("Next")).on_press(PageMessage::Next))
                    )
            )
            .into();
    }
}
