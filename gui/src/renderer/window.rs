// Copyright 2021 Yuri6037

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

use std::string::String;
use iced::Application;
use iced::executor;
use iced::Clipboard;
use std::sync::Arc;
use iced_futures::Command;
use iced::Element;
use iced::Subscription;
use async_channel::Sender;
use async_channel::Receiver;
use async_channel::SendError;
use iced::Column;
use iced::Align;
use iced::Text;

use super::queue::MessageQueue;
use super::welcome::WelcomePage;
use super::error::ErrorPage;
use super::processing::ProcessingPage;
use super::home::HomePage;
use super::finish::FinishPage;
use crate::messages;
use crate::messages::RenderMessage;
use crate::messages::ThreadMessage;
use crate::error::GuiError;

pub trait ConstructiblePage
{
    type PageType : Page + 'static;
    fn new(page: &messages::Page) -> Self::PageType;
}

pub trait Page
{
    fn handle_message(&mut self, msg: &RenderMessage, sender: &mut Sender<ThreadMessage>) -> Result<(), SendError<ThreadMessage>>;
    fn update(&mut self, msg: &PageMessage, _: &mut Sender<ThreadMessage>) -> Result<bool, SendError<ThreadMessage>>;
    fn view(&mut self) -> Element<PageMessage>;
}

#[derive(Clone)]
pub enum PageMessage
{
    Next,
    Close,
    UserInstallSelected(bool),
    InstallFlagSelected(bool),
    TextChanged(String),
    ToggleComponent(usize)
}

#[derive(Clone)]
pub enum GuiMessage
{
    MainThread(Arc<RenderMessage>),
    Page(PageMessage)
}

impl std::fmt::Debug for GuiMessage
{
    fn fmt(&self, _: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error>
    {
        return Ok(());
    }
}

pub struct InstallerWindow
{
    sender: Sender<ThreadMessage>,
    receiver: Receiver<RenderMessage>,
    page: Option<Box<dyn Page>>,
    exit: bool
}

impl InstallerWindow
{
    fn switch_page<TPage: ConstructiblePage>(&mut self, page: &messages::Page)
    {
        self.page = Some(Box::new(TPage::new(page)));
    }

    fn enum_to_type(&mut self, pagemsg: &messages::Page)
    {
        match pagemsg
        {
            messages::Page::Welcome(_, _, _) => self.switch_page::<WelcomePage>(pagemsg),
            messages::Page::Error(_) => self.switch_page::<ErrorPage>(pagemsg),
            messages::Page::Processing => self.switch_page::<ProcessingPage>(pagemsg),
            messages::Page::ComponentView(_, _, _) => self.switch_page::<HomePage>(pagemsg),
            messages::Page::Finish(_) => self.switch_page::<FinishPage>(pagemsg)
        }
    }
}

impl Application for InstallerWindow
{
    type Executor = executor::Default;
    type Message = GuiMessage;
    type Flags = (Sender<ThreadMessage>, Receiver<RenderMessage>);

    fn new((sender, receiver): Self::Flags) -> (Self, Command<GuiMessage>)
    {
        return (
            InstallerWindow
            {
                sender: sender,
                receiver: receiver,
                page: None,
                exit: false
            },
            Command::none()
        );
    }

    fn title(&self) -> String
    {
        return String::from("Installer Framework");
    }

    fn update(&mut self, msg: GuiMessage, _: &mut Clipboard) -> Command<GuiMessage>
    {
        match msg
        {
            GuiMessage::MainThread(msg) =>
            {
                if let RenderMessage::SwitchPage(new_page) = msg.as_ref()
                {
                    self.enum_to_type(new_page);
                }
                if let Some(page) = &mut self.page
                {
                    if let Err(e) = page.handle_message(&msg, &mut self.sender)
                    {
                        self.switch_page::<ErrorPage>(&messages::Page::Error(GuiError::channel_send(e)));
                    }
                }
            },
            GuiMessage::Page(msg) =>
            {
                if let Some(page) = &mut self.page
                {
                    match page.update(&msg, &mut self.sender)
                    {
                        Err(e) => self.switch_page::<ErrorPage>(&messages::Page::Error(GuiError::channel_send(e))),
                        Ok(v) => self.exit = v
                    };
                }
            }
        };
        return Command::none();
    }

    fn should_exit(&self) -> bool
    {
        return self.exit;
    }

    fn view(&mut self) -> Element<GuiMessage>
    {
        if let Some(page) = &mut self.page
        {
            let container = page.view();
            return Column::new()
                .spacing(10)
                .padding(10)
                .align_items(Align::Center)
                .push(container.map(GuiMessage::Page))
                .into();
        }
        else
        {
            return Column::new()
                .spacing(10)
                .padding(10)
                .align_items(Align::Center)
                .push(Text::new("Initializing..."))
                .into();
        }
    }

    fn subscription(&self) -> Subscription<GuiMessage>
    {
        return Subscription::from_recipe(MessageQueue
        {
            channel: self.receiver.clone()
        }).map(|msg| GuiMessage::MainThread(Arc::new(msg)));
    }
}
