// Copyright 2021 Yuri6037

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

pub use install_framework_base as base;
pub use install_framework_core as core;

/// The unified run system. This function automatically causes the program to terminate with the proper exit code.
/// This is the easiest way to run the installer framework
/// 
/// # Arguments
/// 
/// * `builder` - the builder to run
#[cfg(feature = "cli")]
pub fn run<'a>(builder: core::builder::InstallerBuilder<'a>)
{
    use install_framework_cli::CliInterface;
    core::run::<CliInterface>(builder);
}

/// The unified run system. This function automatically causes the program to terminate with the proper exit code.
/// This is the easiest way to run the installer framework
/// 
/// # Arguments
/// 
/// * `builder` - the builder to run
#[cfg(feature = "gui")]
pub fn run<'a>(builder: core::builder::InstallerBuilder<'a>)
{
    use install_framework_gui::GuiInterface;
    core::run::<GuiInterface>(builder);
}

/// The unified run system. This function automatically causes the program to terminate with the proper exit code.
/// This is the easiest way to run the installer framework
/// 
/// # Arguments
/// 
/// * `builder` - the builder to run
#[cfg(not(any(feature = "gui", feature = "cli")))]
pub fn run<'a>(_: core::builder::InstallerBuilder<'a>)
{
    panic!("This crate is not supposed to be used without any interfaces!");
}
